<?php
// Text
$_['text_success']           = '成功：您已經修改了帳號資訊！';
$_['text_list']              = '屬性清單';
$_['text_add']               = '新增屬性';
$_['text_edit']              = '編輯屬性';

// Error 錯誤訊息
$_['error_warning']         = '警告：資料未正確輸入！';
$_['error_permission']      = '警告：您沒有權限更改客人來源資訊！';
$_['error_title']           = '頁面標題必須在3至64個字之間！';
$_['error_description']     = '頁面內容長度不得少於3個字！';
$_['error_meta_title']      = 'Meta 標題必須在3至255個字之間！';
$_['error_confirm']         = '密碼和確認密碼不符!';

$_['error_account_exists']  = '帳號重覆，請重新設定！';
$_['error_password_exists'] = '密碼和確認密碼不符!';

