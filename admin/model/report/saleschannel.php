<?php
class ModelReportSalesChannel extends Model {

	public function getSalesChannelCnt() {

	}

	/**
	 * [getSalesChannelSumList description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-09-15
	 */
	public function getSalesChannelSumList( $data = array()) {
		// dump( $data) ;
		$column = array();
		if ( isset($data['filter_syear']) && $data['filter_syear'] != "") {
			$column[] = "syear='".$this->db->escape($data['filter_syear'])."'";
		}
		if ( isset($data['filter_smonth']) && $data['filter_smonth'] != "") {
			$column[] = "smonth='".$this->db->escape($data['filter_smonth'])."'";
		}
		if ( isset($data['filter_makecard']) && $data['filter_makecard'] != "") {
			$column[] = "makecard like '%".$this->db->escape($data['filter_makecard'])."%'";
		}
		if ( isset($data['filter_platform']) && $data['filter_platform'] != "") {
			$column[] = "platform='".$this->db->escape($data['filter_platform'])."'";
		}


		$SQLCmd = "SELECT makecard, SUM( commission) commission_total FROM tb_summary " ;
		if(count($column)) {
			$SQLCmd .= "WHERE " . implode(" AND ",$column);
		}
        $SQLCmd .= " GROUP BY makecard ORDER BY makecard ASC" ;
		// dump( $SQLCmd) ;
		return $this->db->query( $SQLCmd)->rows ;
	}

	/**
	 * [getMakecardDetailsList description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-09-27
	 */
	public function getMakecardDetailsList( $data = array()) {
		// dump( $data) ;

		$column = array();
		if ( isset($data['filter_syear']) && $data['filter_syear'] != "") {
			$column[] = "syear='".$this->db->escape($data['filter_syear'])."'";
		}
		if ( isset($data['filter_smonth']) && $data['filter_smonth'] != "") {
			$column[] = "smonth='".$this->db->escape($data['filter_smonth'])."'";
		}
		if ( isset($data['filter_platform']) && $data['filter_platform'] != "") {
			$column[] = "platform='".$this->db->escape($data['filter_platform'])."'";
		}

		$SQLCmd = "SELECT platform, CONCAT(syear, '-', LPAD(smonth, 2, 0), '-', LPAD(sday, 2, 0)) occur_date,
				guest_source, consume, commission
			FROM tb_summary WHERE makecard='{$data['makecard']}' " ;

		if(count($column)) {
			$SQLCmd .= "AND " . implode(" AND ",$column);
		}
		$SQLCmd .= " ORDER BY platform ASC, syear DESC, smonth DESC, sday DESC" ;
		// dump( $SQLCmd) ;
		// dump( $this->db->query( $SQLCmd)->rows) ;
		return $this->db->query( $SQLCmd)->rows ;
	}
}
