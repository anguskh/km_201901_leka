<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
			</div>
			<div class="panel-body">
				<div class="well">
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">平台選擇</label>
								<select name="filter_platform" class="form-control">
									<option value="">請選擇</option>
									<option value="1" <?php if( @$filter_platform == "1"):?>selected<?php endif;?>>昇恆昌</option>
									<option value="2" <?php if( @$filter_platform == "2"):?>selected<?php endif;?>>金坊</option>
								</select>
							</div>
							<div class="form-group">
								<label class="control-label">自由行號碼</label>
								<input type="text" name="filter_makecard" value="<?=@$filter_makecard?>" class="form-control" />
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">年度</label>
								<select name="filter_syear" class="form-control">
									<option value="">請選擇</option>
									<option value="2019" <?php if( @$filter_syear == "2019"):?>selected<?php endif;?>>2019</option>
									<option value="2020" <?php if( @$filter_syear == "2020"):?>selected<?php endif;?>>2020</option>
								</select>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">月份</label>
								<select name="filter_smonth" class="form-control">
									<option value="">請選擇</option>
									<option value="1"  <?php if( @$filter_smonth == "1"):?>selected<?php endif;?>>一月</option>
									<option value="2"  <?php if( @$filter_smonth == "2"):?>selected<?php endif;?>>二月</option>
									<option value="3"  <?php if( @$filter_smonth == "3"):?>selected<?php endif;?>>三月</option>
									<option value="4"  <?php if( @$filter_smonth == "4"):?>selected<?php endif;?>>四月</option>
									<option value="5"  <?php if( @$filter_smonth == "5"):?>selected<?php endif;?>>五月</option>
									<option value="6"  <?php if( @$filter_smonth == "6"):?>selected<?php endif;?>>六月</option>
									<option value="7"  <?php if( @$filter_smonth == "7"):?>selected<?php endif;?>>七月</option>
									<option value="8"  <?php if( @$filter_smonth == "8"):?>selected<?php endif;?>>八月</option>
									<option value="9"  <?php if( @$filter_smonth == "9"):?>selected<?php endif;?>>九月</option>
									<option value="10" <?php if( @$filter_smonth == "10"):?>selected<?php endif;?>>十月</option>
									<option value="11" <?php if( @$filter_smonth == "11"):?>selected<?php endif;?>>十一月</option>
									<option value="12" <?php if( @$filter_smonth == "12"):?>selected<?php endif;?>>十二月</option>
								</select>
							</div>
							<div class="form-group">
								<button type="button" id="button-clear" class="btn btn-danger pull-right" style="margin-left:5px;"><i class="fa fa-eraser"></i> 清除</button>
								<button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-filter"></i> 查詢</button>
							</div>
						</div>
						<div class="col-sm-4">
							&nbsp;
						</div>
						<div class="col-sm-4">
							&nbsp;
						</div>
					</div>
				</div>
				<form method="post" action="" enctype="multipart/form-data" id="form-list">
					<div class="table-responsive">
						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<td width="5%" class="text-center">
										<input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
									</td>
									<?php foreach ($columnNames as $colKey => $colName) : ?>
									<td class="text-center"><?=$colName?></td>
									<?php endforeach ; ?>
									<td class="text-center"><?php echo $column_action; ?></td>
								</tr>
							</thead>
							<tbody>
								<?php if ($listRows) { ?>
								<?php foreach ($listRows as $row) { ?>
								<tr class="validate">
									<td class="text-center"><input type="checkbox" name="selected[]" value="" /></td>
									<?php foreach ($columnNames as $colKey => $colName) : ?>
									<td class="text-center"><?=@$row[$colKey]?></td>
									<?php endforeach ; ?>
									<td class="text-center">
										<button type="button" class="btn btn-primary" data-toggle="tooltip" title="檢視" onclick="openModal('<?=$row['makecard']?>')"><i class="fa fa-eye"></i></button>
									</a>
								</td>
							</tr>
							<?php } ?>
							<?php } else { ?>
							<tr>
								<td class="text-center" colspan="<?=$td_colspan?>"><?php echo $text_no_results; ?></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</form>
			<div class="row">
				<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
				<div class="col-sm-6 text-right"><?php echo $results; ?></div>
			</div>
		</div>
	</div>
	<?php include_once("salesChannel_modal.tpl");?>
</div>
<script src="view/javascript/modal_salesChannel.js" type="text/javascript"></script>
<script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
<style type="text/css">
</style>
<script type="text/javascript">
$('#button-filter').on('click', function() {
	filterSend();
});
// 搜尋
function filterSend(){
	url = '?route=report/saleschannel&token='+getURLVar('token');
	var filter = ['syear', 'smonth', 'makecard', 'platform'];
	for(var i=0;i<filter.length;i++){
		var target = $('[name="filter_'+filter[i]+'"]').val();
		if(target != '') {
			url += '&filter_'+filter[i]+'=' + encodeURIComponent(target);
		}
	}
	location = url;
}
</script>
</div>
<?php echo $footer; ?>