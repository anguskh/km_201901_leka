<div class="modal fade" id="modalDetails" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				<h3 class="modal-title">總退傭明細</h3>
			</div>
			<div class="modal-body">
				<div style="margin-bottom:10px;">
					<h4>
					<i class="fa fa-chevron-right" aria-hidden="true"></i>
					<span id="detail-title"> </span></h4>
				</div>
				<table id="modalRate" class="table table-bordered ">
					<thead>
						<tr>
							<td>通路商主要比例</td>
							<td>司機代駕比例</td>
							<td>金豐接送比例</td>
							<td>其他比例</td>
						</tr>
					<thead>
					<tbody>
					</tbody>
				</table>
				<table id="modalList" class="table table-bordered">
					<thead>
						<tr>
							<td class="text-center">平台</td>
							<td class="text-center">日期</td>
							<td class="text-center">客人來源</td>
							<td class="text-center">購買金額</td>
							<td class="text-center">佣金</td>
							<td class="text-center">帶客人數</td>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				<div class="row text-center">
				</div>
			</div>
		</div>
	</div>
</div>