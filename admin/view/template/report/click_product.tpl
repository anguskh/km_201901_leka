<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
        <h1><?php echo $heading_title; ?></h1>
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
        </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li><a href="<?php echo $album_click;?>"><?php echo $tab_album; ?></a></li>
                    <li class="active"><a href="<?php echo $product_click;?>"><?php echo $tab_product; ?></a></li>
                </ul>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <td width="8%" class="text-center"><?php echo $column_sno; ?></td>
                                <td width="10%" class="text-center"><?php echo $column_product_id; ?></td>
                                <td class="text-left"><?php echo $column_product; ?></td>
                                <td width="10%" class="text-center"><?php echo $column_click; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if ($clicks) { ?>
                        <?php foreach ($clicks as $click) { ?>
                        <tr>
                            <td class="text-center"><?php echo $click['sno']; ?></td>
                            <td class="text-center"><?php echo $click['product_id']; ?></td>
                            <td class="text-left">
                                <img src="<?php echo $click['image']; ?>" class="img-thumbnail" />
                            </td>
                            <td class="text-center">
                                <a href="<?php echo ($click["detail"].'&pid='.$click["product_id"]);?>" target="_blank"><?php echo $click['count']; ?></a>
                            </td>
                        </tr>
                        <?php } ?>
                        <?php } else { ?>
                        <tr>
                            <td class="text-center" colspan="5"><?php echo $text_no_results; ?></td>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript"></script> 
</div>
<?php echo $footer; ?>