<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
        	<div class="pull-right">
                <!--<a data-fancybox data-type="iframe" href="javascript:;" data-src="?route=loka/summary/importForm&token=<?=$token?>" class="btn btn-primary">
					<i class="fa fa-plus"></i> 匯入日誌</a>-->
                <button type="button" id="button-import" class="btn btn-primary"><i class="fa fa-plus"></i> 匯入日誌</button>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <div class="well">
                    <div class="row">
						<div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">年度</label>
                                <select name="filter_syear" class="form-control">
                                    <option value="">請選擇</option>
                                    <option value="2019" <?php if($filter_syear == "2019"):?>selected<?php endif;?>>2019</option>
                                    <option value="2020" <?php if($filter_syear == "2020"):?>selected<?php endif;?>>2020</option>
                                </select>
                            </div>
							<div class="form-group">
								<label class="control-label">平台選擇</label>
								<select name="filter_platform" class="form-control">
									<option value="">請選擇</option>
									<option value="1" <?php if($filter_platform == "1"):?>selected<?php endif;?>>昇恆昌</option>
									<option value="2" <?php if($filter_platform == "2"):?>selected<?php endif;?>>金坊</option>
								</select>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">月份</label>
								<select name="filter_smonth" class="form-control">
									<option value="">請選擇</option>
									<option value="1"  <?php if($filter_smonth == "1"):?>selected<?php endif;?>>一月</option>
									<option value="2"  <?php if($filter_smonth == "2"):?>selected<?php endif;?>>二月</option>
									<option value="3"  <?php if($filter_smonth == "3"):?>selected<?php endif;?>>三月</option>
									<option value="4"  <?php if($filter_smonth == "4"):?>selected<?php endif;?>>四月</option>
									<option value="5"  <?php if($filter_smonth == "5"):?>selected<?php endif;?>>五月</option>
									<option value="6"  <?php if($filter_smonth == "6"):?>selected<?php endif;?>>六月</option>
									<option value="7"  <?php if($filter_smonth == "7"):?>selected<?php endif;?>>七月</option>
									<option value="8"  <?php if($filter_smonth == "8"):?>selected<?php endif;?>>八月</option>
									<option value="9"  <?php if($filter_smonth == "9"):?>selected<?php endif;?>>九月</option>
									<option value="10" <?php if($filter_smonth == "10"):?>selected<?php endif;?>>十月</option>
									<option value="11" <?php if($filter_smonth == "11"):?>selected<?php endif;?>>十一月</option>
									<option value="12" <?php if($filter_smonth == "12"):?>selected<?php endif;?>>十二月</option>
								</select>
							</div>
                            <div class="form-group">
                                <label class="control-label">客人來源</label>
                                <input type="text" name="filter_guest_source" value="<?=@$filter_guest_source?>" class="form-control" />
                            </div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">日期</label>
								<select name="filter_sday" class="form-control">
									<option value="">請選擇</option>
									<?php for ( $i=1 ; $i < 32; $i++) : ?>
									<option value="<?=$i?>" <?php if($filter_sday == $i):?>selected<?php endif;?>><?=$i?></option>
									<?php endfor ; ?>
								</select>
							</div>
                            <button type="button" id="button-clear" class="btn btn-danger pull-right" style="margin-left:5px;"><i class="fa fa-eraser"></i>清除</button>
                            <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-filter"></i>查詢</button>
                            <input type="hidden" id="searchFlag">
						</div>
                    </div>
                </div>
                <form method="post" action="" enctype="multipart/form-data" id="form-order">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <!--
                                <td class="text-center">
                                    <a href="<?php echo $sort_number; ?>" class="<?php echo ($sort=='order_number'?strtolower($order):''); ?>"><?php echo $column_order_number; ?></a>
                                </td>
                                -->
								<td width="1%" class="text-center">
									<input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
								</td>
								<?php foreach ($columnNames as $colKey => $colName) : ?>
								<td class="text-center">
									<?php if ( isset( $colSort[$colKey])) : ?>
										<?php if ( $sort == $colKey) : ?>
										<a href="<?=$colSort[$colKey]?>" class="<?php echo strtolower($order); ?>"><?=$colName?></a>
										<?php else : ?>
										<a href="<?=$colSort[$colKey]?>"><?=$colName?></a>
										<?php endif ; ?>
									<?php else : ?>
									<?=$colName?>
									<?php endif ; ?>
								</td>
								<?php endforeach ; ?>
							</tr>
                        </thead>
                        <tbody>
                            <?php if ($listRows) { ?>
                            <?php foreach ($listRows as $row) { ?>
                            <tr>
                                <td class="text-center"><input type="checkbox" name="selected[]" value="<?php echo $row['idx']; ?>" /></td>
                                <?php foreach ($columnNames as $colKey => $colName) : ?>
                                    <?php $inputSize = isset( $columnSize[$colKey]) ? "size=\"$columnSize[$colKey]\"" : "" ; ?>
    							<td class="text-center">
									<input type="text" name="row[<?=$colKey?>][]" value="<?=$row[$colKey]?>" <?=$inputSize?>>
								</td>
	                            <?php endforeach ; ?>
                            </tr>
                            <?php } ?>
                            <?php } else { ?>
                            <tr>
                                <td class="text-center" colspan="<?=$td_colspan?>"><?php echo $text_no_results; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
        <?php include_once("summary_modal.tpl");?>
    </div>
    <script src="view/javascript/modal_summary.js" type="text/javascript"></script>
    <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
	<style type="text/css">
.fancybox-slide--iframe .fancybox-content {
    width  : 300px;
    height : 600px;
    max-width  : 60%;
    max-height : 80%;
    margin: 0;
}
	</style>
    <script type="text/javascript">
    $('.date').datetimepicker({
        pickTime: false
    });

    $('#button-filter').on('click', function() {
        filterSend();
    });
    $('#button-import').on('click', function() {
        openImportModal();
    });

	// 搜尋
	function filterSend(){
		console.log('test');
		var url = '?route=leka/summary&token=' + getURLVar('token') ;
        var msg = "" ;
		var filter = ['syear', 'smonth', 'sday', 'platform', 'guest_source'];

		if ( $('[name="filter_platform"]').val() == '') {
			msg += "平台 未選擇\n" ;
		}
		if ( $('[name="filter_syear"]').val() == '') {
			msg += "年度 未選擇\n" ;
		}
		// if ( $('[name="filter_smonth"]').val() == '') {
		// 	msg += "月份 未選擇\n" ;
		// }

		for(var i=0;i<filter.length;i++) {
			var target = $('[name="filter_'+filter[i]+'"]').val();
			if(target != ''){
				url += '&filter_'+filter[i]+'=' + encodeURIComponent(target);
			}
		}
		console.log( msg);
		if ( msg == "") {
			location = url + "&searchFlag=1";
		} else {
			alert( msg) ;
		}
	}

    // 清除Search
	$("#button-clear").click(function() {
		console.log('button-clear');
		var filter = ['syear', 'smonth', 'sday', 'platform'];
		for(var i=0;i<filter.length;i++) {
			var tag = $('[name="filter_'+filter[i]+'"]')[0].selectedIndex = 0 ;
			console.log( tag);
		}
	});
    </script>
</div>
<?php echo $footer; ?>