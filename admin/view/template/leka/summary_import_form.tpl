<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
<link href="view/stylesheet/bootstrap.css" type="text/css" rel="stylesheet" />
<link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
<link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="screen" />

<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>

</head>
<body>
	<div id="container">
		<div id="content">
				<div class="container-fluid">
					<h1><?php echo $heading_title; ?></h1>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title"><i class="fa fa-list"></i> <?=$text_title?></h3>
						</div>
						<?php if ($success) { ?>
						<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						</div>
						<?php } ?>
						<?php if ($error_warning) { ?>
						<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						</div>
						<?php } ?>
						<div class="panel-body">
							<form class="form-horizontal" id="form" method="post" action="" enctype="multipart/form-data">

<!-- <div class="form-group required">
	<label class="col-sm-2 control-label">上傳月份</label>
	<div class="col-sm-10">

	</div>
</div> -->
<div class="form-group required">
	<label class="col-sm-2 control-label">檔案上傳</label>
	<div class="col-sm-10">
		<input type="file" name="fileToUpload" id="fileToUpload" class="form-control" />
	</div>
</div>
								<button type="submit" id="bt_submit" form="form" data-toggle="tooltip" title="" class="btn btn-primary"><i class="fa fa-save"></i> 上傳匯入</button>
								<button type="button" id="close" data-toggle="tooltip" title="" class="btn btn-success"><i class="fa fa-close"></i> 關閉</button>
							</form>
						</div>
					</div>
				</div>
		</div>
	</div>
<script type="text/javascript">
$('#close').click(function(event) {
	parent.jQuery.fancybox.getInstance().close();
});

</script>
</body>

<footer id="footer">活動咖 &copy; 2017-2019 All Rights Reserved</footer></div>
</body></html>
