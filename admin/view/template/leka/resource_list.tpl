<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
			<div class="pull-right">
                <button type="button" class="btn btn-primary" data-toggle="tooltip" title="新增" onclick="openNewModal();" ><i class="fa fa-plus"></i> 新增</button>
			</div>

            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div id="ajax_message"></div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="search-name">通路商名稱</label>
                                <input type="text" name="filter_source_name" value="<?php echo $filter_source_name; ?>" placeholder="通路商名稱" class="form-control" />
                            </div>
                        </div>
						<div class="col-sm-4">
							<button type="button" id="button-clear" class="btn btn-danger pull-right" style="margin-left:5px;"><i class="fa fa-eraser"></i> 清除</button>
							<button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-filter"></i> 查詢</button>
						</div>
						<div class="col-sm-4">
							&nbsp;
						</div>
						<div class="col-sm-4">
							&nbsp;
						</div>
                    </div>
                </div>
                <form method="post" action="" enctype="multipart/form-data" id="form-list">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <!--
                                <td class="text-center">
                                    <a href="<?php echo $sort_number; ?>" class="<?php echo ($sort=='order_number'?strtolower($order):''); ?>"><?php echo $column_order_number; ?></a>
                                </td>
                                -->
								<td width="5%" class="text-center">
									<input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
								</td>
								<?php foreach ($columnNames as $colKey => $colName) : ?>
								<td class="text-center">
									<?php if ( isset( $colSort[$colKey])) : ?>
										<?php if ( $sort == $colKey) : ?>
									<a href="<?=$colSort[$colKey]?>" class="<?php echo strtolower($order); ?>"><?=$colName?></a>
										<?php else : ?>
									<a href="<?=$colSort[$colKey]?>" ><?=$colName?></a>
										<?php endif ; ?>
									<?php else : ?>
										<?=$colName?>
									<?php endif ; ?>
								</td>
								<?php endforeach ; ?>
								<td class="text-center"><?php echo $column_action; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($listRows) { ?>
                            <?php foreach ($listRows as $row) { ?>
                            <tr class="validate">
                                <td class="text-center"><input type="checkbox" name="selected[]" value="<?php echo $row['idx']; ?>" /></td>
                                <?php foreach ($columnNames as $colKey => $colName) : ?>
                                	<?php if ( $colKey == "source_name") : ?>
    							<td class="text-center">
									<input type="text" name="row[<?=$row['idx']?>][<?=$colKey?>]" value="<?=$row[$colKey]?>">
								</td>
                                	<?php elseif ( $colKey == "total_rate") : ?>
                                <td class="text-center percent">
                                    &nbsp;
                                </td>
                                    <?php else : ?>
    							<td class="text-center">
									<input type="text" name="row[<?=$row['idx']?>][<?=$colKey?>]" value="<?=$row[$colKey]?>" size="10">
								</td>
                                	<?php endif ; ?>
	                            <?php endforeach ; ?>
                                <?php
                                    if ( $row['account_name'] == "" ) {
                                        $button_edit = "帳號資料未填寫" ;
                                        $btClassStr = "btn-danger" ;
                                    } else {
                                        $button_edit = "編輯" ;
                                        $btClassStr = "btn-primary" ;
                                    }
                                ?>
                                <td class="text-center">
									<!--<a data-fancybox data-type="iframe" href="javascript:;" title="<?php echo $button_edit; ?>" data-toggle="tooltip"
										data-src="?route=leka/resource/editAjaxForm&token=<?=$token?>&idx=<?=$row['idx']; ?>"
										class="btn <?=$btClassStr?> editRow">
										<i class="fa fa-eye"></i>
									</a>-->
                                    <button type="button" class="btn <?=$btClassStr?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" onclick="openModal( <?=$row['idx']; ?>);" ><i class="fa fa-eye"></i></button>
                                </td>
                            </tr>
                            <?php } ?>
                            <?php } else { ?>
                            <tr>
                                <td class="text-center" colspan="<?=$td_colspan?>"><?php echo $text_no_results; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
        <?php include_once("resource_modal.tpl");?>
    </div>
    <script src="view/javascript/modal_resource.js?1" type="text/javascript"></script>
    <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
	<style type="text/css">
.fancybox-slide--iframe .fancybox-content {
    width  : 300px;
    height : 600px;
    max-width  : 60%;
    max-height : 80%;
    margin: 0;
}
	</style>
    <script type="text/javascript">
// 暫用不到
//	$('.date').datetimepicker({
//		pickTime: false
//	});

    $('#button-save').on('click', function() {
    	var validate = true ;
    	$('.validate').each( function () {
    		// console.log( Number($(this).find('td').eq(7).text()));
    		if ( Number($(this).find('td').eq(6).text()) != 100) validate = false ;
    	}) ;

    	if ( validate) {
    		$("#form-list").submit();
    	} else {
    		alert( "退傭比例不完整 請檢查後再儲存") ;
    	}
    });
	$('#button-filter').on('click', function() {
		console.log('button-filter');
		url = '?route=leka/resource&token=<?php echo $token; ?>';
		var source_name = $('[name="filter_source_name"]').val();
		if (source_name) {
			url += '&filter_source_name=' + encodeURIComponent(source_name);
		}

		location = url;
	});
	$("#button-clear").click(function(){
		console.log('button-clear');
		$('[name="filter_source_name"]').val('');
	});

	$( document ).ready(function() {
		$('.validate').each(function(index, el) {
			var total_rate = 0 ;
			var tdMsg = "" ;
			var obj = $(this).find("input") ;
			obj.each( function (idx) {
				if ( idx > 1 ) {
					total_rate += Number($(this).val()) ;
				}
			}) ;

			if ( total_rate == 100) {
				tdMsg = "<span style=\"font-weight:bold; color:blue;\">" + total_rate + "</span>" ;
			} else {
				tdMsg = "<span style=\"font-weight:bold; color:red;\">" + total_rate + "</span>" ;
			}

			$(this).find("td").eq(6).html(tdMsg) ;
		});

	});

	$('.validate').keyup(function(event) {
		var tdMsg = "" ;
		var total_rate = 0 ;
		$(this).find("input").each( function (idx) {
			if ( idx > 1 ) {
				total_rate += Number($(this).val()) ;
			}
		});

		if ( total_rate == 100) {
			tdMsg = "<span style=\"font-weight:bold; color:blue;\">" + total_rate + "</span>" ;
			console.log( $(this).find('input').eq(2).attr("name")) ;
			var url = "" ;
			$(this).find("input").each( function (idx) {
				url += "&" + regColName( $(this).attr("name")) + "=" + $(this).val() ;
			});
			console.log( url.replace('][', "")) ;
			$.ajax({
				url: '?route=leka/resource/updateResourceRate&token=' +  getURLVar('token') + url,
				dataType: 'html',
				success: function(resp) {
					console.log( resp);
					$('#ajax_message').html( resp) ;
				}
			});
		} else {
			tdMsg = "<span style=\"font-weight:bold; color:red;\">" + total_rate + "</span>" ;
		}
		$(this).find("td").eq(6).html(tdMsg) ;
	});

	function regColName( str) {
		var retStr = "" ;
		retStr = str.match('\\]\\[(\\w)*') ;
		if ( retStr) {
			return retStr[0].replace('][', "") ;
		} else {
			return "idx";
		}
	}


    </script>
</div>
<?php echo $footer; ?>