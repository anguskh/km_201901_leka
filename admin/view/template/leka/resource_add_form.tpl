<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
<link href="view/stylesheet/bootstrap.css" type="text/css" rel="stylesheet" />
<link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
<link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="screen" />
</head>
<body>
	<div id="container">
		<div id="content">
				<div class="container-fluid">
					<h1><?php echo $heading_title; ?></h1>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title"><i class="fa fa-list"></i> <?=$text_title?></h3>
						</div>
						<?php if ($error_warning) { ?>
						<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						</div>
						<?php } ?>
						<?php if ($success) { ?>
						<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						</div>
						<?php } ?>
						<div class="panel-body">
							<form class="form-horizontal" id="form" method="post" action="" onsubmit="return validateForm()">
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-source_name">通路商名稱</label>
	<div class="col-sm-10">
		<input type="text" name="source_name" value="<?=$source_name?>" placeholder="通路商名稱" id="input-source_name" class="form-control" />
	</div>
</div>
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-account">帳號</label>
	<div class="col-sm-10">
		<input type="text" name="account" value="<?=$account?>" placeholder="帳號" id="input-account" class="form-control" />
	</div>
</div>
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-email">email</label>
	<div class="col-sm-10">
		<input type="text" name="email" value="<?=$email?>" placeholder="Email" id="input-email" class="form-control" />
	</div>
</div>
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-password">密碼</label>
	<div class="col-sm-10">
		<input type="password" name="password" value="<?=$password?>" placeholder="密碼" id="input-password" class="form-control" />
	</div>
</div>
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-repassword">確認密碼</label>
	<div class="col-sm-10">
		<input type="password" name="repassword" value="<?=$repassword?>" placeholder="確認密碼" id="input-repassword" class="form-control" />
	</div>
</div>

<div class="form-group ">
	<label class="col-sm-2 control-label" for="input-rate_main">通路商主要比例</label>
	<div class="col-sm-10">
		<input type="text" name="rate_main" value="<?=$rate_main?>" placeholder="通路商主要比例" id="input-rate_main" class="form-control" />
	</div>
</div>
<div class="form-group ">
	<label class="col-sm-2 control-label" for="input-rate_deduct">通路商扣除比例</label>
	<div class="col-sm-10">
		<input type="text" name="rate_deduct" value="<?=$rate_deduct?>" placeholder="通路商扣除比例" id="input-rate_deduct" class="form-control" />
	</div>
</div>
<div class="form-group ">
	<label class="col-sm-2 control-label" for="input-rate_kmfun">金豐扣除比例</label>
	<div class="col-sm-10">
		<input type="text" name="rate_kmfun" value="<?=$rate_kmfun?>" placeholder="金豐扣除比例" id="input-rate_kmfun" class="form-control" />
	</div>
</div>
<div class="form-group ">
	<label class="col-sm-2 control-label" for="input-rate_transfer">金豐接送比例</label>
	<div class="col-sm-10">
		<input type="text" name="rate_transfer" value="<?=$rate_transfer?>" placeholder="金豐接送比例" id="input-rate_transfer" class="form-control" />
	</div>
</div>
<div class="form-group ">
	<label class="col-sm-2 control-label" for="input-rate_agency">介紹人(仲介) 比例</label>
	<div class="col-sm-10">
		<input type="text" name="rate_agency" value="<?=$rate_agency?>" placeholder="介紹人(仲介) 比例" id="input-rate_agency" class="form-control" />
	</div>
</div>

<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-receive">領取方式</label>
	<div class="col-sm-10">
		<?php
			$seled1 = ($receive == '1') ? "selected" : "" ;
			$seled2 = ($receive == '2') ? "selected" : "" ;
		?>
		<select name="receive">
			<option value="">-- 請選擇 --</option>
			<option value="1" <?=$seled1?>>匯款</option>
			<option value="2" <?=$seled2?>>現金</option>
		</select>
	</div>
</div>
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-account_name">銀行戶名</label>
	<div class="col-sm-10">
		<input type="text" name="account_name" value="<?=$account_name?>" placeholder="銀行戶名" id="input-account_name" class="form-control" />
	</div>
</div>
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-bank_name">銀行名稱</label>
	<div class="col-sm-10">
		<input type="text" name="bank_name" value="<?=$bank_name?>" placeholder="銀行名稱" id="input-bank_name" class="form-control" />
	</div>
</div>
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-bank_branch">分行</label>
	<div class="col-sm-10">
		<input type="text" name="bank_branch" value="<?=$bank_branch?>" placeholder="分行" id="input-bank_branch" class="form-control" />
	</div>
</div>
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-bank_code">銀行代碼</label>
	<div class="col-sm-10">
		<input type="text" name="bank_code" value="<?=$bank_code?>" placeholder="銀行代碼" id="input-bank_code" class="form-control" />
	</div>
</div>
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-bank_account">銀行帳號</label>
	<div class="col-sm-10">
		<input type="text" name="bank_account" value="<?=$bank_account?>" placeholder="銀行帳號" id="input-bank_account" class="form-control" />
	</div>
</div>
								<button type="submit" id="close" form="form" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
							</form>
						</div>
					</div>
				</div>
		</div>
	</div>
<script type="text/javascript">
$('#close').click(function(event) {
	// parent.jQuery.fancybox.getInstance().close();
});

function validateForm() {
	console.log( "validateForm") ;
	var msgStr = "" ;
	$("input[name*='rate']").each( function (idx) {
		console.log( $(this).attr("name"));
		console.log( $(this).val());
		if ($(this).val() != "" && isNaN( $(this).val)) {
			msgStr = "退傭比例必需為數字" ;
			console.log( msgStr);
		}
	}) ;
	return true ;
}


</script>
</body>

<footer id="footer">活動咖 &copy; 2017-2019 All Rights Reserved</footer></div>
</body></html>
