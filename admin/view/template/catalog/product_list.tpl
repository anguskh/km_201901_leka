<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                <a href="<?php echo $upload; ?>" data-toggle="tooltip" title="<?php echo $button_batch_upload; ?>" class="btn btn-default"><i class="fa fa-upload"></i></a>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-product').submit() : false;"><i class="fa fa-trash-o"></i></button>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-model"><?php echo $entry_album; ?></label>
                                <select id="filter_album" name="filter_album" class="form-control">
                                    <option value="">請選擇</option>
                                    <?php foreach($albumArr as $k => $v){?>
                                    <option value="<?php echo $k;?>" <?php if($k==$filter_album){?>selected<?php }?>><?php echo $v;?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                                <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-start"><?php echo $entry_start; ?></label>
                                <div class="input-group date">
                                    <input type="text" name="filter_start" value="<?php echo $filter_start; ?>" placeholder="<?php echo $entry_start; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-bib"><?php echo $entry_bib; ?></label>
                                <input type="text" name="filter_bib" value="<?php echo $filter_bib; ?>" placeholder="<?php echo $entry_bib; ?>" id="input-bib" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-end"><?php echo $entry_end; ?></label>
                                <div class="input-group date">
                                    <input type="text" name="filter_end" value="<?php echo $filter_end; ?>" placeholder="<?php echo $entry_end; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-category"><?php echo $entry_category; ?></label>
                                <select id="filter_category" name="filter_category" class="form-control">
                                    <option value="">請選擇</option>
                                    <?php foreach($categoryArr as $k => $v){?>
                                    <option value="<?php echo $k;?>" <?php if($k==$filter_category){?>selected<?php }?>><?php echo $v;?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <button type="button" id="button-clear" class="btn btn-danger pull-right" style="margin-left:5px;"><i class="fa fa-eraser"></i> <?php echo $button_clear; ?></button>
                            <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-filter"></i> <?php echo $button_filter; ?></button>
                        </div>
                    </div>
                </div>
                <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-product">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td width="5%" class="text-center">
                                        <input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
                                    </td>
                                    <td width="8%" class="text-center">
                                        <a href="<?php echo $sort_id; ?>" <?php if($sort == 'p.product_id'){?>class="<?php echo strtolower($order); ?>"<?php }?>><?php echo $column_id; ?></a>
                                    </td>
                                    <td width="10%" class="text-center"><?php echo $column_image; ?></td>
                                    <td width="18%" class="text-center">
                                        <a href="<?php echo $sort_album; ?>" <?php if($sort == 'p.album_id'){?>class="<?php echo strtolower($order); ?>"<?php }?>><?php echo $column_album; ?></a>
                                    </td>
                                    <td class="text-center"><?php echo $column_bib; ?></td>
                                    <td width="10%" class="text-center"><?php echo $column_photographer; ?></td>
                                    <td width="12%" class="text-center">
                                        <a href="<?php echo $sort_price; ?>" <?php if($sort == 'p.price'){?>class="<?php echo strtolower($order); ?>"<?php }?>><?php echo $column_price; ?></a>
                                    </td>
                                    <td width="10%" class="text-center"><?php echo $column_status; ?></td>
                                    <td width="10%" class="text-center"><?php echo $column_action; ?></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($products) { ?>
                                <?php foreach ($products as $product) { ?>
                                <tr>
                                    <td class="text-center">
                                        <input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" <?php if (in_array($product['product_id'], $selected)){?>checked="checked"<?php }?> />
                                    </td>
                                    <td class="text-center"><?php echo $product['product_id']; ?></td>
                                    <td class="text-center">
                                        <?php if ($product['image']) { ?>
                                        <div class="imageItem" data-mediasrc="<?php echo $product['orgin_img'];?>" data-mediawidth="<?php echo $product['img_width'];?>" data-mediaheight="<?php echo $product['img_height'];?>">
                                            <img src="<?php echo $product['image']; ?>" class="img-thumbnail" />
                                        </div>
                                        <?php } else { ?>
                                        <span class="img-thumbnail list"><i class="fa fa-camera fa-2x"></i></span>
                                        <?php } ?>
                                    </td>
                                    <td class="text-center"><?php echo (isset($albumArr[$product['album_id']])? $albumArr[$product['album_id']] : ''); ?></td>
                                    <td class="text-center"><?php echo (isset($bibArr[$product['product_id']])? implode("、",$bibArr[$product['product_id']]) : ''); ?></td>
                                    <td class="text-center"><?php echo $product['photographer']; ?></td>
                                    <td class="text-center"><?php echo '$'.ceil($product['price'])."/".$product['points']."點"; ?></td>
                                    <td class="text-center"><?php echo $product['status']; ?></td>
                                    <td class="text-center"><a href="<?php echo $product['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                                </tr>
                                <?php } ?>
                                <?php } else { ?>
                                <tr>
                                    <td class="text-center" colspan="9"><?php echo $text_no_results; ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
    <div id="imageModal" class="modal fade">
        <div class="modal-dialog text-center">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="" id="itemImage"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $('.date').datetimepicker({
        pickTime: false
    });
    $('#button-filter').on('click', function() {
        var url = 'index.php?route=catalog/product&token=<?php echo $token; ?>';
        
        var filter_name = $('input[name=\'filter_name\']').val();
        if (filter_name) {
            url += '&filter_name=' + encodeURIComponent(filter_name);
        }
        var filter_bib = $('input[name=\'filter_bib\']').val();
        if (filter_bib) {
            url += '&filter_bib=' + encodeURIComponent(filter_bib);
        }
        var filter_album = $('#filter_album').val();
        if (filter_album) {
            url += '&filter_album=' + encodeURIComponent(filter_album);
        }
        var filter_start = $('[name="filter_start"]').val();
        if (filter_start) {
            url += '&filter_start=' + encodeURIComponent(filter_start);
        }
        var filter_end = $('[name="filter_end"]').val();
        if (filter_end) {
            url += '&filter_end=' + encodeURIComponent(filter_end);
        }
        var filter_category = $('[name="filter_category"]').val();
        if (filter_category) {
            url += '&filter_category=' + encodeURIComponent(filter_category);
        }
        location = url;
    });
    $("#button-clear").click(function(){
        $('[name="filter_name"]').val('');
        $('[name="filter_bib"]').val('');
        $('#filter_album').val('');
        $('[name="filter_start"]').val('');
        $('[name="filter_end"]').val('');
        $('[name="filter_category"]').val('');
    });
    $(".imageItem").click(function(){
        var imageSrc    = $(this).data('mediasrc');
		var imageWidth  = $(this).data('mediawidth');
        $('#itemImage').attr('src',imageSrc);
        if(imageWidth==0 || imageWidth>550){
            $('#itemImage').css({width:550});
        }
        else{
            $('#itemImage').css({width:imageWidth});
        }
        $("#imageModal").modal('show');
    });
    </script>
</div>
<?php echo $footer; ?>