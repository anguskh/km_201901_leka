<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-order-num"><?php echo $entry_order_number; ?></label>
                                <input type="text" name="order_num" value="<?php echo $order_num; ?>" placeholder="<?php echo $entry_order_number; ?>" id="input-order-num" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-customer"><?php echo $entry_customer_acc; ?></label>
                                <input type="text" name="customer_acc" value="<?php echo $customer_acc; ?>" placeholder="<?php echo $entry_customer_acc; ?>" id="input-customer" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-order-status"><?php echo $entry_order_status; ?></label>
                                <select name="o_status" id="input-order-status" class="form-control">
                                    <option value="">請選擇</option>
                                    <?php foreach ($order_statuses as $order_status) { ?>
                                    <option value="<?php echo $order_status["order_status_id"];?>" <?php if($order_status["order_status_id"]==$o_status){?>selected<?php }?>><?php echo $order_status["name"];?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-pay-status"><?php echo $entry_pay_status; ?></label>
                                <select name="p_status" id="input-pay-status" class="form-control">
                                    <option value="">請選擇</option>
                                    <?php foreach ($pay_statuses as $k => $v) { ?>
                                    <option value="<?php echo $k;?>" <?php if($k==$p_status){?>selected<?php }?>><?php echo $v;?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-date-added"><?php echo $entry_date_added; ?></label>
                                <div class="input-group date">
                                <input type="text" name="sdate" value="<?php echo $sdate; ?>" placeholder="<?php echo $entry_date_added; ?>" data-date-format="YYYY-MM-DD" id="input-date-added" class="form-control" />
                                <span class="input-group-btn">
                                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                </span></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-date-modified"><?php echo $entry_date_modified; ?></label>
                                <div class="input-group date">
                                <input type="text" name="edate" value="<?php echo $edate; ?>" placeholder="<?php echo $entry_date_modified; ?>" data-date-format="YYYY-MM-DD" id="input-date-modified" class="form-control" />
                                <span class="input-group-btn">
                                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                </span></div>
                            </div>
                            <button type="button" id="button-clear" class="btn btn-danger pull-right" style="margin-left:5px;"><i class="fa fa-eraser"></i> <?php echo $button_clear; ?></button>
                            <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-filter"></i> <?php echo $button_filter; ?></button>
                        </div>
                    </div>
                </div>
                <form method="post" action="" enctype="multipart/form-data" id="form-order">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <td class="text-center">
                                    <a href="<?php echo $sort_number; ?>" class="<?php echo ($sort=='order_number'?strtolower($order):''); ?>"><?php echo $column_order_number; ?></a>
                                </td>
                                <td class="text-center">
                                    <a href="<?php echo $sort_account; ?>" class="<?php echo ($sort=='customer_acc'?strtolower($order):''); ?>"><?php echo $column_customer_acc; ?></a>
                                </td>
                                <td class="text-center">
                                    <a href="<?php echo $sort_name; ?>" class="<?php echo ($sort=='customer_name'?strtolower($order):''); ?>"><?php echo $column_customer_name; ?></a>
                                </td>
                                <td class="text-center">
                                    <a href="<?php echo $sort_price; ?>" class="<?php echo ($sort=='price'?strtolower($order):''); ?>"><?php echo $column_order_price; ?></a>
                                </td>
                                <td class="text-center">
                                    <a href="<?php echo $sort_ostatus; ?>" class="<?php echo ($sort=='order_status'?strtolower($order):''); ?>"><?php echo $column_status; ?></a>
                                </td>
                                <td class="text-center">
                                    <a href="<?php echo $sort_pstatus; ?>" class="<?php echo ($sort=='pay_status'?strtolower($order):''); ?>"><?php echo $column_pay_status; ?></a>
                                </td>
                                <td class="text-center">
                                    <a href="<?php echo $sort_date_added; ?>" class="<?php echo ($sort=='date_added'?strtolower($order):''); ?>"><?php echo $column_date_added; ?></a>
                                </td>
                                <td class="text-center"><?php echo $column_action; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($orders) { ?>
                            <?php foreach ($orders as $order) { ?>
                            <tr>
                                <td class="text-center"><?php echo $order['order_number']; ?></td>
                                <td class="text-center"><?php echo $order['customer_acc']; ?></td>
                                <td class="text-center"><?php echo $order['customer_name']; ?></td>
                                <td class="text-center"><?php echo $order['price']; ?></td>
                                <td class="text-center"><?php echo $order['order_status']; ?></td>
                                <td class="text-center"><?php echo (isset($pay_statuses[$order['pay_status']])?$pay_statuses[$order['pay_status']]:''); ?></td>
                                <td class="text-center"><?php echo $order['date_added']; ?></td>
                                <td class="text-center">
                                    <a href="<?php echo $order['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                </td>
                            </tr>
                            <?php } ?>
                            <?php } else { ?>
                            <tr>
                                <td class="text-center" colspan="9"><?php echo $text_no_results; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
    <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
    <script type="text/javascript">
    $('.date').datetimepicker({
        pickTime: false
    });
    $('#button-filter').on('click', function() {
        url = 'index.php?route=sale/order&token=<?php echo $token; ?>';
    
        var order_num = $('[name="order_num"]').val();
        if (order_num) {
            url += '&order_num=' + encodeURIComponent(order_num);
        }
        var customer_acc = $('[name="customer_acc"]').val();
        if (customer_acc) {
            url += '&customer_acc=' + encodeURIComponent(customer_acc);
        }
        var order_status = $('[name="o_status"]').val();
        if (order_status != '') {
            url += '&o_status=' + encodeURIComponent(order_status);
        }
        var p_status = $('[name="p_status"]').val();
        if (p_status != '') {
            url += '&p_status=' + encodeURIComponent(p_status);
        }
        var sdate = $('[name="sdate"]').val();
        if (sdate) {
            url += '&sdate=' + encodeURIComponent(sdate);
        }
        var edate = $('[name="edate"]').val();
        if (edate) {
            url += '&edate=' + encodeURIComponent(edate);
        }
    
        location = url;
    });
    $("#button-clear").click(function(){
        $('[name="order_num"]').val('');
        $('[name="customer_acc"]').val('');
        $('[name="o_status"]').val('');
        $('[name="p_status"]').val('');
        $('[name="sdate"]').val('');
        $('[name="edate"]').val('');
    });    
    </script> 
</div>
<?php echo $footer; ?> 