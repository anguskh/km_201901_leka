<?php
/**
 * 通路傭金表
 * @Another Angus
 * @date    2019-09-14
 */
class ControllerReportSalesChannel extends Controller {
	private $func_path     = "report/saleschannel" ;

	/**
	 * [index description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-09-14
	 */
	public function index() {
		$this->document->setTitle("通路傭金表 - 樂咖") ;
		$this->load->model( $this->func_path) ;

		$this->getList() ;
	}

	/**
	 * [getList description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-09-14
	 */
	protected function getList() {

		// dump( $this->request->get) ;
		if ( isset( $this->session->data['success'])) {
			$data['success'] = $this->session->data['success'] ;
			unset( $this->session->data['success']) ;
		} else {
			$data['success'] = '' ;
		}
		// if ( isset( $this->session->data['error_warning'])) {
		// 	$data['error_warning'] = $this->session->data['error_warning'] ;
		// 	unset( $this->session->data['error_warning']) ;
		// } else {
		// 	$data['error_warning'] = '' ;
		// }


		// 整理列表搜尋條件
		$filterArr = array(
			"page"            => "頁碼",
			"filter_limit"    => "分頁數",

			"filter_syear"    => "年度",
			"filter_smonth"   => "月份",
			"filter_makecard" => "自由行號碼",
			"filter_platform" => "平台選擇",
		);

        foreach($filterArr as $col => $name){
            $data["{$col}"] = isset($this->request->get["{$col}"]) ? trim($this->request->get["{$col}"]) : "";

            // if(isset($this->request->get["{$col}"]) && !empty($this->request->get["{$col}"])){
            //     $this->urlArr["default"] .=  "&{$col}=".$this->request->get["{$col}"];
            //     if($col!="page"){
            //         $this->urlArr["page"] .= "&{$col}=".$this->request->get["{$col}"];
            //     }
            // }
        }

		$data['heading_title']   = "通路傭金表";
		$data['error_warning']   = "" ;
		// $data['success']      = "" ;
		$data['pagination']      = "" ;
		$data['results']         = "" ;
		$data['text_list']       = "列表" ;
		$data['text_no_results'] = $this->language->get('text_no_results') ;
		$data['column_action']   = "動作" ;
		$data['button_edit']     = "編輯" ;
		$data['token']           = $this->session->data['token'] ;

		// 設定列表欄位 ---------------------------------------------------------------------------------
		$columnNames = array(
				"source_name"      => '通路商名稱',
				"makecard"         => '自由行號碼',
				"commission_total" => '退傭金額',
			) ;

		$data['columnNames'] = $columnNames ;
		$data['td_colspan']  = count( $columnNames) + 2 ;


		// 麵包屑 Start ---------------------------------------------------------------------------------
		$data['breadcrumbs'] = array();
		$url = "" ;
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => "通路傭金表",
			'href' => $this->url->link('report/saleschannel', 'token=' . $this->session->data['token'] . $url, true)
		);
		// 麵包屑 End ---------------------------------------------------------------------------------

		// 查詢
		$filter_data = array (
			'filter_syear'    => $data["filter_syear"],
			'filter_smonth'   => $data["filter_smonth"],
			'filter_makecard' => $data["filter_makecard"],
			'filter_platform' => $data["filter_platform"],

			'start'           => ($data["page"] - 1) * $data["filter_limit"],
			'limit'           => $data["filter_limit"]
		);


		$data['listRows'] = $this->model_report_saleschannel->getSalesChannelSumList( $filter_data) ;
		// dump( $data['listRows']) ;


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('report/salesChannel_list', $data));
	}

	/**
	 * [ajaxListMakecardDetails description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-09-27
	 */
	public function ajaxListMakecardDetails() {
		// dump( $this->request->get) ;
		$this->load->model( $this->func_path) ;
		$filter_data = array(
			"makecard"        => isset( $this->request->get['makecard_name']) ? $this->request->get['makecard_name'] : "",
			"filter_syear"    => isset( $this->request->get['filter_syear']) ? $this->request->get['filter_syear'] : "",
			"filter_smonth"   => isset( $this->request->get['filter_smonth']) ? $this->request->get['filter_smonth'] : "",
			"filter_platform" => isset( $this->request->get['filter_platform']) ? $this->request->get['filter_platform'] : "",
		) ;
		$retRows = $this->model_report_saleschannel->getMakecardDetailsList( $filter_data) ;

		$htmlStr = "" ;
		$platformName = array( "1" => "昇恆昌", "2" => "金坊") ;
		foreach ($retRows as $iCnt => $row) {
			$htmlStr .= "<tr><td class='text-center'>{$platformName[$row['platform']]}</td>" ;
			$htmlStr .= "<td class='text-center'>{$row['occur_date']}</td>" ;
			$htmlStr .= "<td class='text-center'>{$row['guest_source']}</td>" ;
			$htmlStr .= "<td class='text-center'>{$row['consume']}</td>" ;
			$htmlStr .= "<td class='text-center'>{$row['commission']}</td></tr>" ;
		}

		echo $htmlStr ;
	}
}
