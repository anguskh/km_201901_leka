<?php
// https://fontawesome.com/v4.7.0/cheatsheet/
class ControllerCommonColumnLeft extends Controller {
	public function index() {
        if(isset($this->request->get['act']) && $this->request->get['act']=='ajax'){
            echo $this->change();
            exit;
        }
		if (isset($this->request->get['token']) && isset($this->session->data['token']) && ($this->request->get['token'] == $this->session->data['token'])) {
			$this->load->language('common/column_left');

			$this->load->model('user/user');

			$this->load->model('tool/image');

			$user_info = $this->model_user_user->getUser($this->user->getId());

			if ($user_info) {
				$data['firstname']  = $user_info['firstname'];
				$data['lastname']   = $user_info['lastname'];
				$data['username']   = $user_info['username'];
				$data['user_group'] = $user_info['user_group'];

				if (is_file(DIR_IMAGE . $user_info['image'])) {
					$data['image'] = $this->model_tool_image->resize($user_info['image'], 45, 45);
				} else {
					$data['image'] = '';
				}
			} else {
				$data['firstname'] = '';
				$data['lastname'] = '';
				$data['user_group'] = '';
				$data['image'] = '';
			}


			// add by Angus 2019.01.25
			// $data['menus'][] = array(
			// 		'id'       => 'menu-news',
			// 		'icon'	   => 'fa-newspaper-o',
			// 		'name'	   => "公告管理",
			// 		'href'     => $this->url->link('leka/news', 'token=' . $this->session->data['token'], true),
			// 		'children' => array()
			// );
			$data['menus'][] = array(
					'id'       => 'menu-summary',
					'icon'	   => 'fa-dashboard',
					'name'	   => "資訊總覽",
					'href'     => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true),
					'children' => array()
			);
			$data['menus'][] = array(
					'id'       => 'menu-summary',
					'icon'	   => 'fa-database',
					'name'	   => "退傭日誌管理",
					'href'     => $this->url->link('leka/summary', 'token=' . $this->session->data['token'], true),
					'children' => array()
			);
			$data['menus'][] = array(
					'id'       => 'menu-resource',
					'icon'	   => 'fa-users',
					'name'	   => "客人來源管理",
					'href'     => $this->url->link('leka/resource', 'token=' . $this->session->data['token'], true),
					'children' => array()
			);
			// $data['menus'][] = array(
			// 		'id'       => 'menu-agent',
			// 		'icon'	   => 'fa-book',
			// 		'name'	   => "介紹人管理",
			// 		'href'     => $this->url->link('leka/agent', 'token=' . $this->session->data['token'], true),
			// 		'children' => array()
			// );

			// 報表管理
			$report = array() ;
			$report[] = array(
				'name'	   => "總退傭表",
				'href'     => $this->url->link('report/refund', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);
			$report[] = array(
				'name'	   => "通路傭金表",
				'href'     => $this->url->link('report/saleschannel', 'token=' . $this->session->data['token'], true),
				'children' => array()
			);


			$data['menus'][] = array(
					'id'       => 'menu-report',
					'icon'	   => 'fa-bar-chart',
					'name'	   => "報表管理",
					'href'     => "",
					'children' => $report
			);

			/**
			 *	權限管理
			 * 		使用者管理
			 * 		群組帳號
			 */
			// 使用者管理
			$user = array();
			if ($this->user->hasPermission('access', 'user/user')) {
				$user[] = array(
					'name'	   => $this->language->get('text_user'),
					'href'     => $this->url->link('user/user', 'token=' . $this->session->data['token'], true),
					'children' => array()
				);
			}
			// 群組帳號
			if ($this->user->hasPermission('access', 'user/user_permission')) {
				$user[] = array(
					'name'	   => $this->language->get('text_user_group'), // 群組帳號
					'href'     => $this->url->link('user/user_permission', 'token=' . $this->session->data['token'], true),
					'children' => array()
				);
			}
			// 最後把功能放入meun
			if ($user) {
                $data['menus'][] = array(
					'id'       => 'menu-user',
					'icon'	   => 'fa-user',
					'name'	   => $this->language->get('text_users'), // 權限管理
					'href'     => '',
					'children' => $user
				);
			}

            //左邊選單預設打開
            $data["col_left"] = 'active';
            if(isset($this->session->data['colLeft'])){
                $data["col_left"] = $this->session->data['colLeft'];
            }

            return $this->load->view('common/column_left', $data);
		}
	}

    public function change(){
        if(!isset($this->session->data['colLeft']) || $this->session->data['colLeft']=='active'){
            $this->session->data['colLeft'] = '';
        }
        else{
            $this->session->data['colLeft'] = 'active';
        }

        return $this->session->data['colLeft'];
    }
}
