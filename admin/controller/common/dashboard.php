<?php
/**
 * 資訊總覽
 * http://localhost/leka/admin/?route=common/dashboard&token=uU8ZBilaJXQ8OUNJvRdyFZ9lr7agyFh9
 * 總消費金額 Total consumption amount
 * 年度累積金額 Annual cumulative amount
 * 佣金累計 Commission accumulation
 * 指定商品 Designated product
 */
class ControllerCommonDashboard extends Controller {
	public function index() {
		// dump( $this->config) ;

		$this->load->language('common/dashboard');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['heading_title'] = $this->language->get('heading_title');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		// 資訊總覽 add by Angus 2019.10.21
		// 目前以先寫死功能來處理
		// 總消費金額、年度累積金額、佣金累計、指定商品
		// $output = $this->load->controller('extension/dashboard/' . $code . '/dashboard'); // 範例

		// Dashboard Extensions
		$dashboards = array();
		// $output = $this->load->controller('extension/dashboard/consumption/dashboard');
		// // dump( $output) ;
		// if ($output) {
		// 	$dashboards[] = array(
		// 		// 'code'       => $code,
		// 		// 'width'      => $this->config->get('dashboard_' . $code . '_width'),
		// 		// 'sort_order' => $this->config->get('dashboard_' . $code . '_sort_order'),
		// 		'code'       => "consumption",
		// 		'width'      => 4,
		// 		'sort_order' => 0,
		// 		'output'     => $output
		// 	);
		// }
		// $output = $this->load->controller('extension/dashboard/commission/dashboard');
		// if ($output) {
		// 	$dashboards[] = array(
		// 		// 'code'       => $code,
		// 		// 'width'      => $this->config->get('dashboard_' . $code . '_width'),
		// 		// 'sort_order' => $this->config->get('dashboard_' . $code . '_sort_order'),
		// 		'code'       => "commission",
		// 		'width'      => 4,
		// 		'sort_order' => 0,
		// 		'output'     => $output
		// 	);
		// }
		// $output = $this->load->controller('extension/dashboard/desproduct/dashboard');
		// if ($output) {
		// 	$dashboards[] = array(
		// 		// 'code'       => $code,
		// 		// 'width'      => $this->config->get('dashboard_' . $code . '_width'),
		// 		// 'sort_order' => $this->config->get('dashboard_' . $code . '_sort_order'),
		// 		'code'       => "desproduct",
		// 		'width'      => 4,
		// 		'sort_order' => 0,
		// 		'output'     => $output
		// 	);
		// }
		$output = $this->load->controller('extension/dashboard/recent/dashboard');
		// dump( $output) ;
		if ($output) {
			$dashboards[] = array(
				// 'code'       => $code,
				// 'width'      => $this->config->get('dashboard_' . $code . '_width'),
				// 'sort_order' => $this->config->get('dashboard_' . $code . '_sort_order'),
				'code'       => "recent",
				'width'      => 12,
				'sort_order' => 0,
				'output'     => $output[0]
			);
			$dashboards[] = array(
				// 'code'       => $code,
				// 'width'      => $this->config->get('dashboard_' . $code . '_width'),
				// 'sort_order' => $this->config->get('dashboard_' . $code . '_sort_order'),
				'code'       => "recent",
				'width'      => 12,
				'sort_order' => 0,
				'output'     => $output[1]
			);
		}


		// 組合 Dashboard Extensions --------------------------------------------------------------
		$width = 0;
		$column = array();
		$data['rows'] = array();
		// dump( $dashboards) ;
		foreach ($dashboards as $dashboard) {
			$column[] = $dashboard;

			$width = ($width + $dashboard['width']);
			// dump( $width) ;
			if ($width >= 12) {
				$data['rows'][] = $column;

				$width = 0;
				$column = array();
			}
		}
		$data['rows'][] = $column;

		$data['header']         = $this->load->controller('common/header');
		$data['column_left']    = $this->load->controller('common/column_left');
		$data['footer']         = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('common/dashboard', $data));
	}
}