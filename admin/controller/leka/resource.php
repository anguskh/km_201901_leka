<?php
class ControllerLekaResource extends Controller {
	private $error = array() ;
	private $func_path     = "leka/resource" ;

	/**
	 * [index description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-01-25
	 */
	public function index() {
		// $this->document->setTitle($this->language->get('heading_title'));
		$this->document->setTitle("客人來源管理 - 樂咖") ;

		$this->load->model( $this->func_path) ;
		$this->load->language( $this->func_path) ;

		$this->getList() ;
	}

	/**
	 * [modalResourceDetails description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-09-30
	 */
	public function modalResourceDetails() {
		$this->load->model( $this->func_path) ;

		$filter_data = array (
			"idx" => $this->request->get['idx']
		) ;

		$infoRetArr = $this->model_leka_resource->getInfoForIdx( $filter_data) ;
		// dump( $infoRetArr) ;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($infoRetArr));
	}

	/**
	 * [addForm description]
	 * @Another Angus
	 * @date    2019-10-01
	 */
	public function addForm() {
		// dump( $this->request->post) ;
		$this->load->model( $this->func_path) ;
		$this->load->language( $this->func_path) ;

		if ( $this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateForm()) {
			$insInfo = $this->request->post ;
			$this->model_leka_resource->addResource( $insInfo) ;
			$this->session->data['success'] = $this->language->get('text_success');
		}

		$this->getList() ;
	}

	/**
	 * [editForm description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-10-01
	 */
	public function editForm() {
		// dump( $this->request->post) ;
		$this->load->model( $this->func_path) ;
		$this->load->language( $this->func_path) ;

		if ( $this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateEditForm()) {
			// dump( $this->request->post) ;
			$upInfo = $this->request->post ;
			$this->model_leka_resource->editResource( $upInfo) ;
			if ( !empty( $upInfo['rDetails_account'])) {
				$this->model_leka_resource->editAccount( $upInfo) ;
			}

			$this->session->data['success'] = $this->language->get('text_success');
		}

		$this->getList() ;
	}

	/**
	 * [updateResourceRate description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-05
	 */
	public function updateResourceRate() {
		$this->load->model( $this->func_path) ;
		$this->load->language( $this->func_path) ;

		if ( $this->validateUpdateForm()) {
			$data[ $this->request->get['idx']] = array(
				"source_name"   => $this->request->get['source_name'],
				"main_rate"     => $this->request->get['main_rate'],
				"kmfun_rate"    => $this->request->get['kmfun_rate'],
				"transfer_rate" => $this->request->get['transfer_rate'],
				"other_rate"    => $this->request->get['other_rate'],
			) ;
			// dump( $data) ;
			$this->model_leka_resource->editRowSource( $data) ;
			$source_name = $this->request->get['source_name'] ;
			echo '<div class="alert alert-success"><i class="fa fa-check-circle"></i>
			通路商 : '.$source_name.' 退傭比例更新完成
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>' ;
		}
	}

	/**
	 * [getList description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-01-25
	 */
	protected function getList() {
		// if ( $this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateListForm()) {
		// 	// dump( $this->request->post) ;
		// 	// exit() ;
		// 	$editRow = $this->request->post['row'] ;
		// 	$this->model_leka_resource->editRowSource( $editRow) ;
		// 	$this->session->data['success'] = $this->language->get('text_success');
		// }
		if ( isset( $this->session->data['success'])) {
			$data['success'] = $this->session->data['success'] ;
			unset( $this->session->data['success']) ;
		} else {
			$data['success'] = '' ;
		}
		if ( isset( $this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'] ;
			foreach ($this->error as $eKey => $msg) {
				if ( $eKey != "warning") {
					$data['error_warning'] .= "<br>" .$this->error[$eKey] ;
				}
			}
			unset( $this->error['warning']) ;
		} else {
			$data['error_warning'] = '' ;
		}

		$data['heading_title']   = "客人來源管理";
		// $data['success']      = "" ;
		$data['pagination']      = "" ;
		$data['results']         = "" ;
		$data['text_list']       = "列表" ;
		$data['text_no_results'] = $this->language->get('text_no_results') ;
		$data['column_action']   = "動作" ;
		$data['button_edit']     = "編輯" ;
		$data['token']           = $this->session->data['token'] ;


		// 設定列表欄位 ---------------------------------------------------------------------------------
		// $columnNames = array(
		// 		"source_name"   => '通路商名稱',
		// 		"main_rate"     => '通路商主要比例',
		// 		"deduct_rate"   => '通路商扣除比例',
		// 		"kmfun_rate"    => '金豐扣除比例',
		// 		"transfer_rate" => '金豐接送比例',
		// 		"agency_rate"   => '介紹人(仲介) 比例',
		// 		"total_rate"    => '驗証',
		// 	) ;
		// 2019.11.05 調整
		$columnNames = array(
				"source_name"    => '通路商名稱',
				"main_rate"      => '通路商主要比例',
				// "deduct_rate" => '通路商扣除比例',
				"kmfun_rate"     => '司機代駕比例',
				"transfer_rate"  => '金豐接送比例',
				// "agency_rate" => '介紹人(仲介) 比例',
				"other_rate"     => '其他比例',
				"total_rate"     => '驗証',
			) ;

		// 取得排序資訊
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'default';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		$data['columnNames'] = $columnNames ;
		$data['td_colspan']  = count( $columnNames) + 2 ;


		// 麵包屑 Start ---------------------------------------------------------------------------------
		$data['breadcrumbs'] = array();
		$url = "" ;
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => "客人來源管理",
			'href' => $this->url->link('leka/resource', 'token=' . $this->session->data['token'] . $url, true)
		);
		// 麵包屑 End ---------------------------------------------------------------------------------

		// 欄位排序
		$url = '';
		if (isset($this->request->get['filter_source_name'])) {
			$url .= '&filter_source_name=' . urlencode(html_entity_decode($this->request->get['filter_source_name'], ENT_QUOTES, 'UTF-8')) ;
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		$data['colSort']['source_name']   = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=source_name' . $url, true);
		$data['colSort']['main_rate']     = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=main_rate' . $url, true);
		$data['colSort']['deduct_rate']   = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=deduct_rate' . $url, true);
		$data['colSort']['kmfun_rate']    = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=kmfun_rate' . $url, true);
		$data['colSort']['transfer_rate'] = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=transfer_rate' . $url, true);
		$data['colSort']['agency_rate']   = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . '&sort=agency_rate' . $url, true);

		$data['filter_source_name'] = isset( $this->request->get['filter_source_name']) ? $this->request->get['filter_source_name'] : "" ;
		$filter_data = array (
			"source_name" => $data['filter_source_name'],

			'sort'        => $sort,
			'order'       => $order,
		);


		$data['listRows'] = $this->model_leka_resource->getResourceList( $filter_data) ;

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('leka/resource_list', $data));
	}

	/**
	 * [validateForm description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-02-13
	 */
	protected function validateForm () {
		// 檢查是否有權限
		if (!$this->user->hasPermission('modify', 'leka/resource')) {
			$this->error['warning'] = $this->language->get('error_permission') ;
		} else {
			// dump( $this->request->post) ;
			// 檢查帳號是否重覆
			$source_id = isset( $this->request->post['nDetails_idx']) ? $this->request->post['nDetails_idx'] : "" ;
			if ( empty($this->request->post['nDetails_account'])) {
				$this->error['account_exists'] = $this->language->get('error_account_exists') ;
			}

			if ( !empty($this->request->post['nDetails_account']) &&
					$this->model_leka_resource->checkAccountExists( $source_id, $this->request->post['nDetails_account'])) {
				$this->error['account_exists'] = $this->language->get('error_account_exists') ;
			}

			if ($this->error && !isset($this->error['warning'])) {
				$this->error['warning'] = $this->language->get('error_warning');
			}
		}
		// dump( $this->error) ;
		return !$this->error;
	}

	/**
	 * [validateForm description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-02-10
	 */
	protected function validateEditForm() {
		// 檢查是否有權限
		if (!$this->user->hasPermission('modify', 'leka/resource')) {
			$this->error['warning'] = $this->language->get('error_permission') ;
		} else {
			// 檢查帳號是否重覆
			$source_id = isset( $this->request->post['rDetails_idx']) ? $this->request->post['rDetails_idx'] : "" ;
			if ( !empty($this->request->post['rDetails_account']) &&
					$this->model_leka_resource->checkAccountExists( $source_id, $this->request->post['rDetails_account'])) {
				$this->error['account_exists'] = $this->language->get('error_account_exists') ;
			}

			if ($this->request->post['rDetails_password'] != $this->request->post['rDetails_repassword']) {
				$this->error['confirm'] = $this->language->get('error_confirm');
			}

			if ($this->error && !isset($this->error['warning'])) {
				$this->error['warning'] = $this->language->get('error_warning');
			}
		}
		return !$this->error;
	}

	/**
	 * [validateUpdateForm description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-06
	 */
	protected function validateUpdateForm() {
		// 檢查是否有權限
		if (!$this->user->hasPermission('modify', 'leka/resource')) {
			$this->error['warning'] = $this->language->get('error_permission') ;
		} else {
			// 檢查新增欄位是否有缺
			// 通路商名稱
			// 帳號
			// 領取方式
			// 密碼
			// 確認密碼
    // [rDetails_source_name] => 金豐Angus1
    // [rDetails_account] => leka_angus1
    // [rDetails_receive] => 1
    // [rDetails_password] => 1234
    // [rDetails_repassword] => 1234
    // [rDetails_email] =>
    // [rDetails_account_name] =>
    // [rDetails_bank_code] =>
    // [rDetails_bank_name] =>
    // [rDetails_bank_branch] =>
    // [rDetails_bank_account] =>
		}
		return !$this->error;
	}

}