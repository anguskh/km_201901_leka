<?php

// 楊育德
// 金亞立
// 通路接送

include_once('inc/common_inc.php') ;
$dbObj = new database( 'anguskh.com', 'kmfun', 'kmfun123', 'leka_dev', '3306') ;

include_once('inc/PHPExcel.php');

$objPHPExcel = PHPExcel_IOFactory::load("2importResourceAccount.xlsx") ;
$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

$SQLCmd = "SELECT idx, source_name FROM tb_resource " ;
$retArr = $dbObj->query( $SQLCmd)->rows ;
$sourArr = array() ;
foreach ($retArr as $iCnt => $row) {
	$sourArr[$row['idx']] = $row['source_name'] ;
}
// print_r( $sourArr) ;

foreach($sheetData as $index => $rowCols) {
	// if ( $index == 258) break ;

	$insArr = array() ;
	if ( $index > 1) {
		$insArr['source_name']  = trim($rowCols['A']) ;

		if ( trim($rowCols['B']) == "匯款" ) {
			$insArr['receive'] = 1 ;
		} else if ( trim($rowCols['B']) == "現金" ) {
			$insArr['receive'] = 2 ;
		} else {
			$insArr['receive'] = "" ;
		}

		$insArr['account_name'] = trim($rowCols['C']) ;
		$insArr['bank_name']    = trim($rowCols['D']) ;
		$insArr['bank_branch']  = trim($rowCols['G']) ;
		$insArr['bank_code']    = trim($rowCols['E']) ;
		$insArr['bank_account'] = trim($rowCols['F']) ;

		// print_r( $insArr) ;
		$source_idx = "" ;
		foreach ($sourArr as $idx => $source_name) {
			if ( $insArr['source_name'] == $source_name) {
				$source_idx = $idx ;
			}
		}
		if ( !empty( $source_idx)) {
			unset( $insArr['source_name']) ;
			$SQLCmd = makeSqlCmd( $insArr, $source_idx) ;
			// print_r( $SQLCmd) ;
			if ( !empty( $SQLCmd)) {
				$dbObj->query( $SQLCmd) ;
			}
		} else {
			echo "{$insArr['source_name']}<br>" ;
		}

	}
}


function makeSqlCmd( $data = array(), $source_id = "") {
	$insStr = "" ;
	foreach ($data as $colKey => $tmpStr) {
		if ( !empty( $tmpStr)) {
			$insStr .= "{$colKey}='{$tmpStr}'," ;
		}
	}
	$insStr = substr( $insStr, 0, strlen( $insStr)-1) ;
	if ( !empty( $insStr)) {
		return "update tb_resource set {$insStr} where idx={$source_id} " ;
	} else {
		return "" ;
	}

}


exit() ;