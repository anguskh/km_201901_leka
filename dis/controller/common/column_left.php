<?php
class ControllerCommonColumnLeft extends Controller {
	public function index() {
        if(isset($this->request->get['act']) && $this->request->get['act']=='ajax'){
            echo $this->change();
            exit;
        }
		if (isset($this->request->get['token']) && isset($this->session->data['token']) && ($this->request->get['token'] == $this->session->data['token'])) {
			$this->load->language('common/column_left');

			$this->load->model('user/user');

			$this->load->model('tool/image');


			// $user_info = $this->model_user_user->getUser($this->user->getId());
			// dump( $this->user->isLogged()) ;
			if ( $this->user->isLogged()) {
				$data['username'] = $this->user->getUserName() ;
				$data['account']  = $this->user->getAccount() ;
				$data['image']    = '';
 				// if (is_file(DIR_IMAGE . $user_info['image'])) {
				// 	$data['image'] = $this->model_tool_image->resize($user_info['image'], 45, 45);
				// } else {
				// 	$data['image'] = '';
				// }
			} else {
				$data['firstname'] = '';
				$data['lastname'] = '';
				$data['user_group'] = '';
				$data['image'] = '';
			}
			// 資訊總覽
			$data['menus'][] = array(
					'id'       => 'menu-summary',
					'icon'	   => 'fa-dashboard',
					'name'	   => "資訊總覽",
					'href'     => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true),
					'children' => array()
			);
			// 退傭單位報表
			$data['menus'][] = array(
					'id'       => 'menu-report',
					'icon'	   => 'fa-bar-chart',
					'name'	   => "個人報表檢視",
					'href'     => $this->url->link('report/personal', 'token=' . $this->session->data['token'], true),
					'children' => ""
			);

			$data['menus'][] = array(
					'id'       => 'menu-user',
					'icon'	   => 'fa-user',
					'name'	   => "個人檔案",
					'href'     => $this->url->link('user/profile', 'token=' . $this->session->data['token'], true),
					'children' => ""
			);

			// 權限管理
			// $user = array();
			// $user[] = array(
			// 	'name'	   => $this->language->get('text_user'),
			// 	'href'     => $this->url->link('user/profile', 'token=' . $this->session->data['token'], true),
			// 	'children' => array()
			// );
			// if ($user) {
			// 	$data['menus'][] = array(
			// 		'id'       => 'menu-user',
			// 		'icon'	   => 'fa-user',
			// 		'name'	   => $this->language->get('text_users'),
			// 		'href'     => '',
			// 		'children' => $user
			// 	);
			// }


            //左邊選單預設打開
            $data["col_left"] = 'active';
            if(isset($this->session->data['colLeft'])){
                $data["col_left"] = $this->session->data['colLeft'];
            }

            return $this->load->view('common/column_left', $data);
		}
	}

    public function change(){
        if(!isset($this->session->data['colLeft']) || $this->session->data['colLeft']=='active'){
            $this->session->data['colLeft'] = '';
        }
        else{
            $this->session->data['colLeft'] = 'active';
        }

        return $this->session->data['colLeft'];
    }
}
