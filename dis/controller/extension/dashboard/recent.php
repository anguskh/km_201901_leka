<?php
/**
 * 資訊總覽 DIS 前台
 * http://localhost/leka/admin/?route=common/dashboard&token=uU8ZBilaJXQ8OUNJvRdyFZ9lr7agyFh9
 * 總消費金額 Total consumption amount
 * 年度累積金額 Annual cumulative amount
 * 佣金累計 Commission accumulation
 * 指定商品 Designated product
 * add by Angus 2019.10.21
*/
class ControllerExtensionDashboardRecent extends Controller {
	public function dashboard() {
		$this->load->model( 'tool/report') ;

		$filter_data = array (
			"source_name"     => $this->user->getUserName(),
		);

		$retArr = $this->model_tool_report->dashboardTotal( $filter_data) ;
		// dump( $retArr) ;

		$platformArr = array(
			"1" => "昇恆昌",
			"2" => "金坊",
		) ;
		$data = array() ;
		$output = array() ;
		foreach ($retArr as $iCnt => $row) {
			$data['platform']   = $platformArr[$row['platform']] ;
			$data['consume']    = number_format( $row['t_consume']) ;
			$data['commission'] = number_format( $row['t_commission']) ;
			$data['appoint']    = number_format( $row['t_appoint']) ;
			$data['mcnt']       = number_format( $row['mcnt']) ;
			$output[] = $this->load->view('extension/dashboard/recent_all', $data);
			$data = array() ;
		}

		return $output ;
	}
}
