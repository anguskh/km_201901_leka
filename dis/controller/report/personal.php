<?php
class ControllerReportPersonal extends Controller {
	/**
	 * [index description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-01-30
	 */
	public function index() {
		$this->document->setTitle("退傭表 - 樂咖") ;
		// dump( $this->user) ;

		// $this->load->model('loka/resource') ;

		$this->getList() ;
	}

	/**
	 * [getList description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-05
	 */
	protected function getList() {
		// dump( $this->user->rateInfo) ;

		// dump( $this->request->get) ;
		$data['heading_title']   = "退傭表";
		$data['error_warning']   = "" ;
		$data['success']         = "" ;
		$data['pagination']      = "" ;
		$data['results']         = "" ;
		$data['text_list']       = "列表" ;
		$data['text_no_results'] = $this->language->get('text_no_results') ;
		$data['column_action']   = "動作" ;
		$data['button_edit']     = "編輯" ;
		$data['token']           = $this->session->data['token'] ;

		$this->load->model('tool/report') ;

		// 設定列表欄位 ---------------------------------------------------------------------------------
		$columnNames = array(
				"syear"                => '年度',
				"smonth"               => '月份',
				"sum_consume"          => '當月總購物金',
				"main_rate_calcul"     => '通路實拿佣金',
				"kmfun_rate_calcul"    => '司機代駕傭金',
				"transfer_rate_calcul" => '金豐接送傭金',
				"other_rate_calcul"    => '其他傭金',
				"cnt"                  => '次數',
				"mcnt"                 => '總人數',
			) ;

		$data['columnNames'] = $columnNames ;
		$data['td_colspan']  = count( $columnNames) + 2 ;


		$data['breadcrumbs'] = array();
		$url = "" ;
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => "退傭表",
			'href' => $this->url->link('report/personal', 'token=' . $this->session->data['token'] . $url, true)
		);

		// 整理列表搜尋條件
		$filterArr = array(
			"page"            => "頁碼",
			"filter_limit"    => "分頁數",

			"filter_syear"    => "年度",
			"filter_platform" => "平台選擇",
		);

		foreach($filterArr as $col => $name){
			$data["{$col}"] = isset($this->request->get["{$col}"]) ? trim($this->request->get["{$col}"]) : "";
		}

		$filter_data = array (
			'filter_syear'    => $data["filter_syear"],
			'filter_platform' => $data["filter_platform"],
			"source_name"     => $this->user->getUserName(),
		);

		$refundRoleArr = $this->model_tool_report->getRefundLists( $filter_data) ;
		// dump( $refundRoleArr) ;
		$data['listRows'] = $refundRoleArr ;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('report/all_refund_report', $data));
	}

	/**
	 * [monthDetail 退傭表 列表 - 日報表]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-11-05
	 */
	public function monthDetail() {
		// dump( $this->request->get) ;
		$data['heading_title']   = "退傭表";
		$data['error_warning']   = "" ;
		$data['success']         = "" ;
		$data['pagination']      = "" ;
		$data['results']         = "" ;
		$data['text_list']       = "列表 - 日報表" ;
		$data['text_no_results'] = $this->language->get('text_no_results') ;
		$data['column_action']   = "動作" ;
		$data['button_cancel']   = "回上頁";
		$data['token']           = $this->session->data['token'] ;

		$this->load->model('tool/report') ;

		// 設定列表欄位 ---------------------------------------------------------------------------------
				// "source_name"          => '通路商名稱',
				// "sum_commission"       => '總傭金',
				// "main_rate_calcul"     => '通路實拿佣金',
				// "kmfun_rate_calcul"    => '司機代駕傭金',
				// "transfer_rate_calcul" => '金豐接送傭金',
				// "other_rate_calcul"    => '其他傭金',
				// "cnt"                  => '筆數',
				// "mcnt"                 => '總人數',
		$columnNames = array(
				"syear"                => '年度',
				"smonth"               => '月份',
				"sday"                 => '日期',
				"cardno"               => '製卡人員',
				"consume"              => '當月總購物金',
				"main_rate_calcul"     => '通路實拿佣金',
				"kmfun_rate_calcul"    => '司機代駕傭金',
				"transfer_rate_calcul" => '金豐接送傭金',
				"other_rate_calcul"    => '其他傭金',
				"people"               => '人數',
			) ;

		$data['columnNames'] = $columnNames ;
		$data['td_colspan']  = count( $columnNames) + 2 ;


		$data['breadcrumbs'] = array();
		$url = "" ;
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => "退傭表",
			'href' => $this->url->link('report/personal', 'token=' . $this->session->data['token'] . $url, true)
		);
		$data['breadcrumbs'][] = array(
			'text' => "日報表",
			'href' => $this->url->link('report/personal/monthDetail', 'token=' . $this->session->data['token'] . $url, true)
		);
		$data['cancel'] = $this->url->link('report/personal', 'token=' . $this->session->data['token'] . $url, true);

		// dump( $this->request->get) ;
		// 整理列表搜尋條件
		$filterArr = array(
			"filter_syear"    => "年度",
			"filter_smonth"    => "月份",
			"filter_platform" => "平台選擇",
		);

		foreach($filterArr as $col => $name){
			$data["{$col}"] = isset($this->request->get["{$col}"]) ? trim($this->request->get["{$col}"]) : "";
		}

		$filter_data = array (
			'filter_platform' => $data["filter_platform"],
			'filter_syear'    => $data["filter_syear"],
			'filter_smonth'   => $data["filter_smonth"],

			"source_name"     => $this->user->getUserName(),
		);

		switch ($data["filter_platform"]) {
			case '1':
				$data['platformName'] = "昇恆昌" ;
				break;
			case '2':
				$data['platformName'] = "金坊" ;
				break;

			default:
				$data['platformName'] = "全平台" ;
				break;
		}

		$refundRoleArr = $this->model_tool_report->getList( $filter_data) ;
		$data['listRows'] = $refundRoleArr ;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('report/all_refund_report_month', $data));
	}

}
