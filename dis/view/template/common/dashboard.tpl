<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php //dump( $rows) ; ?>
        <?php foreach ($rows as $row) : ?>
        <div class="row">
            <div class="nav-scroller col-12">
                <nav class="nav d-flex"><!-- nav d-flex Start -->
                <?php foreach ($row as $dashboard_1) : ?>
                    <?php if ( $dashboard_1['width'] == 14 ) : ?>
                        <?php echo $dashboard_1['output']; ?>
                    <?php else : ?>
                        <?php $class = 'col-lg-' . $dashboard_1['width'] . ' col-md-3 col-sm-6'; ?>
                    <?php foreach ($row as $dashboard_2) : ?>
                        <?php if ($dashboard_2['width'] > 3) { ?>
                            <?php $class = 'col-lg-' . $dashboard_1['width'] . ' col-md-12 col-sm-12'; ?>
                        <?php } ?>
                    <?php endforeach ; ?>
                    <div class="<?php echo $class; ?>"><?php echo $dashboard_1['output']; ?></div>
                    <?php endif ; ?>
                <?php endforeach ; ?>
                </nav><!-- nav d-flex End -->
            </div>
        </div>
        <?php endforeach ; ?>
    </div>
</div>
<?php echo $footer; ?>