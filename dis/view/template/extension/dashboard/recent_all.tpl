<h3><?=$platform?></h3>
<div class="col-lg-3 col-md-4 col-sm-6">
	<div class="tile">
		<div class="tile-heading">年度總消費金額累計<?php //echo $heading_title; ?></div>
		<div class="tile-body"><i class="fa fa-users"></i>
			<h3 class="pull-right "><?php echo $consume; ?></h3>
		</div>
		<div class="tile-footer"><a href="">檢視明細...<?php //echo $text_view; ?></a></div>
	</div>
</div>
<div class="col-lg-3 col-md-4 col-sm-6">
	<div class="tile">
		<div class="tile-heading">年度佣金累計<?php //echo $heading_title; ?></div>
		<div class="tile-body"><i class="fa fa-users"></i>
			<h3 class="pull-right"><?php echo $commission; ?></h3>
		</div>
		<div class="tile-footer"><a href="">檢視明細...<?php //echo $text_view; ?></a></div>
	</div>
</div>
<div class="col-lg-3 col-md-4 col-sm-6">
	<div class="tile">
		<div class="tile-heading">年度指定商品累計<?php //echo $heading_title; ?></div>
		<div class="tile-body"><i class="fa fa-users"></i>
			<h3 class="pull-right"><?php echo $appoint; ?></h3>
		</div>
		<div class="tile-footer"><a href="">檢視明細...<?php //echo $text_view; ?></a></div>
	</div>
</div>
<div class="col-lg-3 col-md-4 col-sm-6">
	<div class="tile">
		<div class="tile-heading">年度人數<?php //echo $heading_title; ?></div>
		<div class="tile-body"><i class="fa fa-users"></i>
			<h3 class="pull-right"><?php echo $mcnt; ?></h3>
		</div>
		<div class="tile-footer"><a href="">檢視明細...<?php //echo $text_view; ?></a></div>
	</div>
</div>
