<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
			<div class="pull-right">
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <div class="well">
                    <div class="row">
						<div class="col-sm-4">
							<div class="form-group">
                                <?=$platformName?>
							</div>
						</div>
						<div class="col-sm-4">
                            <div class="form-group">
                            </div>
						</div>
						<div class="col-sm-4">
                            <div class="form-group">
                            </div>
						</div>
                    </div>
                </div>
                <form method="post" action="" enctype="multipart/form-data" id="form-order">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <!--
                                <td class="text-center">
                                    <a href="<?php echo $sort_number; ?>" class="<?php echo ($sort=='order_number'?strtolower($order):''); ?>"><?php echo $column_order_number; ?></a>
                                </td>
                                -->
								<?php foreach ($columnNames as $colKey => $colName) : ?>
								<td class="text-center">
									<?php if ( isset( $colSort[$colKey])) : ?>
										<?php if ( $sort == $colKey) : ?>
										<a href="<?=$colSort[$colKey]?>" class="<?php echo strtolower($order); ?>"><?=$colName?></a>
										<?php else : ?>
										<a href="<?=$colSort[$colKey]?>"><?=$colName?></a>
										<?php endif ; ?>
									<?php else : ?>
									<?=$colName?>
									<?php endif ; ?>
								</td>
								<?php endforeach ; ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($listRows) { ?>
                            <?php foreach ($listRows as $row) { ?>
                            <tr class="viewDetail">
                                <!-- <td class="text-center"><input type="checkbox" name="selected[]" value="<?php echo $row['idx']; ?>" /></td> -->
                                <?php foreach ($columnNames as $colKey => $colName) : ?>
                                    <?php if ( $colKey != "syear" && $colKey != "smonth" && $colKey != "sday" && $colKey != "cardno") : ?>
                                <td class="text-center"><?=number_format($row[$colKey])?></td>
                                    <?php else : ?>
                                <td class="text-center"><?=$row[$colKey]?></td>
                                    <?php endif ; ?>
	                            <?php endforeach ; ?>
                            </tr>
                            <?php } ?>
                            <?php } else { ?>
                            <tr>
                                <td class="text-center" colspan="<?=$td_colspan?>"><?php echo $text_no_results; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
    <script src="dis/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <link href="dis/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
	<style type="text/css">
	</style>
    <script type="text/javascript">
	$('.date').datetimepicker({
		pickTime: false
	});
    // $('.viewDetail').click( function ()) {
    //     console.log('test');
    // });
	$('#button-filter').on('click', function() {
		console.log('button-filter');
		url = 'index.php?route=report/personal&token=<?php echo $token; ?>';
		var search_year = $('[name="search_year"]').val();
		if (search_year) {
			url += '&search_year=' + encodeURIComponent(search_year);
		}

		location = url;
	});
    </script>
</div>
<?php echo $footer; ?>