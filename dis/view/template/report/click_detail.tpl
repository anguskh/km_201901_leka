<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
        <h1><?php echo $heading_title; ?></h1>
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
        </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <?php if($album_id!=""){?>
                <ul class="nav nav-tabs">
                    <li class="active"><a href="<?php echo $album_click;?>"><?php echo $tab_album; ?></a></li>
                    <li><a href="<?php echo $product_click;?>"><?php echo $tab_product; ?></a></li>
                </ul>
                <?php }?>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <td width="10%" class="text-center"><?php echo $column_date; ?></td>
                                <?php foreach($timeColumn as $time){?>
                                <td width="3%" class="text-center"><?php echo $time; ?></td>
                                <?php }?>
                                <td width="10%" class="text-center"><?php echo $column_cnt; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="bg-info">
                                <td class="text-center"><?php echo $column_total; ?></td>
                                <?php 
                                    $sumTotal = 0;
                                    foreach($totals as $k => $v){
                                        $sumTotal += $v;
                                ?>
                                <td class="text-center"><?php echo $v; ?></td>
                                <?php }?>
                                <td class="text-center"><?php echo $sumTotal; ?></td>
                            </tr>
                            <?php foreach ($results as $date => $time) { ?>
                            <tr>
                                <td class="text-center"><?php echo $date."(".$weekData[date("w",strtotime($date))].")"; ?></td>
                                <?php
                                    $sumTotal = 0;
                                    foreach($time as $k => $v){
                                        $sumTotal += $v;
                                ?>
                                <td class="text-center"><?php echo $v; ?></td>
                                <?php }?>
                                <td class="text-center"><?php echo $sumTotal; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript"></script> 
</div>
<?php echo $footer; ?>