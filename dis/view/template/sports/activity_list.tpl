<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success">
            <i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
            	<div class="well">
            		<div class="row">
            			<div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label" for="input-sdate"><?php echo $searchNames["sdate"]; ?></label>
                                <div class="input-group date" id="sdate">
                                    <input type="text" name="sdate" value="<?php echo $sdate; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-edate"><?php echo $searchNames["edate"]; ?></label>
                                <div class="input-group date" id="edate">
                                    <input type="text" name="edate" value="<?php echo $edate; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label"><?php echo $searchNames["flag"]; ?></label>
                                <select name="flag" class="form-control">
                                    <option value="" <?php if($flag==""):?>selected<?php endif;?>>請選擇</option>
                                    <option value="0" <?php if($flag==0 && $flag!=""):?>selected<?php endif;?>>否</option>
                                    <option value="1" <?php if($flag==1):?>selected<?php endif;?>>是</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-search"><?php echo $searchNames["search"]; ?></label>
                                <input type="text" name="search" value="<?php echo $search; ?>" id="input-search" class="form-control" />
                            </div>
                            <button type="button" id="button-clear" class="btn btn-danger pull-right" style="margin-left:5px;"><i class="fa fa-eraser"></i> <?php echo $button_clear; ?></button>
                            <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-filter"></i> <?php echo $button_filter; ?></button>
                        </div>
            		</div>
            	</div>
				<div class="table-responsive">
					<table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <?php foreach ($columnNames as $key => $colName) : ?>
                                <td class="text-center">
                                    <a href="" class=""><?php echo $colName; ?></a>
                                </td>
                                <?php endforeach ; ?>
                                <td class="text-center"><?php echo $column_tags; ?></td>
                                <td class="text-center"><?php echo $column_action; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if(count($results)) :?>
                        <?php foreach ( $results as $iCnt => $tmpRow) : ?>
                            <tr>
                                <?php foreach ($columnNames as $key => $colName) : ?>
                                <td class="text-center"><?php echo $tmpRow[$key]; ?></td>
                                <?php endforeach ; ?>
                                <td class="text-center"><?php echo implode("、",$tags[$tmpRow["seqno"]]); ?></td>
                                <td class="text-center">
                                    <a href="<?php echo $tmpRow['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                </td>
                            </tr>
                        <?php endforeach ; ?>
                        <?php else :?>
                        <tr>
                            <td class="text-center" colspan="<?php echo count($columnNames)+2;?>"><?php echo $text_no_results; ?></td>
                        </tr>
                        <?php endif ;?>
                        </tbody>
					</table>
				</div>
				<div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $indexDesc; ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('#sdate,#edate').datetimepicker({
        pickTime: false
    });
    $('#button-filter').on('click', function() {
        var url = 'index.php?route=sports/activity&token=<?php echo $token; ?>';
        var sdate = $('input[name=\'sdate\']').val();
        if (sdate) {
            url += '&sdate=' + encodeURIComponent(sdate);
        }
        var edate = $('input[name=\'edate\']').val();
        if (edate) {
            url += '&edate=' + encodeURIComponent(edate);
        }
        var search = $('input[name=\'search\']').val();
        if (search) {
            url += '&search=' + encodeURIComponent(search);
        }
        var flag = $('[name=\'flag\']').val();
        if (flag) {
            url += '&flag=' + encodeURIComponent(flag);
        }
        location = url;
    });
    $("#button-clear").click(function(){
        $('input[name="sdate"]').val('');
        $('input[name="edate"]').val('');
        $('input[name="search"]').val('');
        $('[name="flag"]').val('');
    });
});
</script>
<?php echo $footer; ?>