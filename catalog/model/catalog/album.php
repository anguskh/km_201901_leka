<?php
class ModelCatalogAlbum extends Model{
    
    /**
	 * [getTotalCnt 取相簿列表]
	 * @param   array   $data [description]
	 * @return  [type]        [description]
	 * @Another Nicole
	 * @date    2018-03-29
	 */
	public function getLists($data = array()) {
        $filterArr = array();
        
        if(isset($data["status"])){
            $filterArr[] = "a.status=".intval($data["status"]);
        }
        if(isset($data["search"]) && !empty($data["search"])){
            $filterArr[] = "a.name like '%".$this->db->escape($data["search"])."%'";
        }
        if(isset($data["activity"]) && !empty($data["activity"])){
            $filterArr[] = "a.activityseqno=".intval($data["activity"]);
        }
        
        $filterStr = "1=1";
        if(count($filterArr)){
            $filterStr = implode(" AND ",$filterArr);
        }
        
        $SQLCmd = "SELECT a.*,p2.image as cover_image,p1.image as first_image ".
            "FROM ". DB_PREFIX ."album a JOIN (SELECT album_id,image from ". DB_PREFIX ."product where status=1 group by album_id) p1 using(album_id) ".
            "LEFT JOIN ". DB_PREFIX ."product p2 on(a.cover_id=p2.product_id) ".
            "WHERE {$filterStr} ORDER BY a.shoot_start desc" ;
        
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$SQLCmd .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $SQLCmd;exit;
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}
    
    /**
	 * [getAlbum 取相簿內容]
	 * @param   string  $album_id   [description]
	 * @return  [type]              [description]
	 * @Another Nicole
	 * @date    2018-03-30
	 */
	public function getAlbum( $album_id) {
		$SQLCmd = "SELECT a.*,p.image as cover_image ".
            "FROM ". DB_PREFIX ."album a LEFT JOIN ". DB_PREFIX ."product p using(album_id) ".
            "WHERE a.album_id=".intval($album_id)." AND a.status=2" ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->row;
	}
    
    /**
	 * [getAlbumCovers 取相簿封面]
	 * @param   array  $albums   [description]
	 * @return  [type]              [description]
	 * @Another Nicole
	 * @date    2018-04-18
	 */
    public function getAlbumCovers( $albums){
        $SQLCmd = "SELECT a.album_id,p2.image as cover_image,p1.image as first_image ".
            "FROM ". DB_PREFIX ."album a JOIN (SELECT album_id,image from ". DB_PREFIX ."product where status=1 group by album_id) p1 using(album_id) ".
            "LEFT JOIN ". DB_PREFIX ."product p2 on(a.cover_id=p2.product_id) ".
            "WHERE a.album_id in (".implode(",",$albums).") AND a.status=2" ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows;
    }
}
