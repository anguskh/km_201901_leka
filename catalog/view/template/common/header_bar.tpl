<div class="clearfix">
	<a class="iconToggle" href="#">
		<img height="27" src="catalog/view/image/icon_toggle.png" alt="menu" title="" />
	</a>
    <h1 class="logo">
		<a href="https://www.eventpal.com.tw">
			<img src="catalog/view/image/logo.png" alt="活動咖 Eventpal" title="" />
		</a>
	</h1>
	<h1 class="m_logo">
		<a class="link icon-LOGO" href="https://www.eventpal.com.tw" itemprop="url">
			<span class="text" itemprop="name">活動咖 EventPal</span>
		</a>
	</h1>
	<nav>
		<ul class="navList clearfix">
            <li class="navItem">
				<a class="navLink navHome" href="#">回首頁</a>
			</li>
			<li class="navItem">
		    	<a class="navLink navIcon iconPhone" href="https://www.eventpal.com.tw/FOAS/actions/Index.action?about">活動咖服務說明</a>
			</li>
			<li class="navItem">
		    	<a class="navLink navIcon iconShopping" href="https://www.eventpal.com.tw/FOAS/actions/IndexNew.action?initType=shopping">購物咖</a>
			</li>
			<li class="navItem">
		    	<a class="navLink navIcon iconCamera active" href="http://photo.eventpal.com.tw" target="_blank">相片咖</a>
			</li>
			<li class="navItem ">
		    	<a class="navLink navIcon iconSport" href="http://sports.eventpal.com.tw/" target="_blank">運動咖</a>
			</li>
			<!--li class="navItem">
		    	<a class="navLink navIcon iconLogin" href="#">會員登入</a>
			</li-->
			<li class="navItem">
		    	<a class="navLink navLine iconLine" href="https://line.me/R/ti/p/%40oes9401w" target="_blank">
		    		<img height="36" src="catalog/view/image/icon_line_add.jpg" alt="加入 LINE 好友" title="" />
                    <span>加入 LINE 好友</span>
		    	</a>
			</li>
		</ul>
	</nav>
</div>